#!/usr/bin/env node

const argv = require('yargs')
  .usage('Usage: $0 --directory <path> <command> [options]')
  .demandCommand()
  .commandDir('commands')
  .describe('directory', 'Directory of your .MOVs')
  .demandOption(['directory'])
  .describe('watermark', 'Path to a watermark file')
  .describe('vimeo-client-id', '')
  .describe('vimeo-client-secret', '')
  .describe('shopify-shop-name', '')
  .describe('shopify-api-key', '')
  .describe('shopify-password', '')
  .demandOption(['directory']).argv;
