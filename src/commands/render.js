/*
 * Includes
 */
const videoModule = require('../modules/video');
const utils = require('../modules/utils');

exports.command = 'render';
exports.describe = 'Renders low resolution video of .MOVs in current directory';
exports.builder = {};
exports.handler = argv => {
  const directory = argv.directory;
  const files = utils.getFiles(directory, videoModule.movieFileRegex);
  utils.createTempDirectory(directory);
  if (argv.directory && argv.watermark) {
    const { watermark } = argv;
    files.map(video => videoModule.renderLowRes(video, watermark));
  }
};
