/*
 * Includes
 */
const videoModule = require('../modules/video');
const utils = require('../modules/utils');
const fs = require('fs');
const path = require('path');

exports.command = 'meta';
exports.describe = 'Reads meta data from a video file and writes it into a JSON file';
exports.builder = {};
exports.handler = argv => {
  const directory = argv.directory;
  const files = utils.getFiles(directory, videoModule.movieFileRegex);
  utils.createTempDirectory(directory);
  files.map(video => {
    // Remove old file
    const basename = path.basename(video.file);
    const metaDataFilePath = path.dirname(video.file) + `/tmp/${basename}.json`;
    const metaDataFileExists = fs.existsSync(metaDataFilePath);
    if (metaDataFileExists) {
      fs.unlinkSync(metaDataFilePath);
    }

    // Fetch meta data and write to JSON file
    videoModule.getMetaData(video).then(metadata =>
      fs.writeFile(metaDataFilePath, JSON.stringify(metadata, null, 4), () => {
        console.log(basename + ': Meta data file written');
      })
    );

    // Create thumbnail as product image
    videoModule.createThumbnail(video);
  });
};
