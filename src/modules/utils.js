const fs = require('fs');
const path = require('path');

const getFiles = (directory, pattern) => {
  let result = [];

  if (!directory || !pattern) {
    return result;
  }

  fs.readdirSync(directory).map(fileName => {
    const absolutePath = path.resolve(directory, fileName);
    const fileStats = fs.statSync(absolutePath);

    // Recursively call this function, if current file is a directory
    if (fileStats.isDirectory()) {
      const tmp = getFiles(absolutePath, pattern);
      result = result.concat(tmp);
    }

    // Compute file path and file size, if current file is a file
    if (fileStats.isFile() && pattern.test(fileName)) {
      const fileSizeInMegaBytes = bytesToSize(fileStats.size);
      result.push({ file: absolutePath, size: fileSizeInMegaBytes });
    }
  });

  return result;
};

const createTempDirectory = directory => {
  const tempDir = directory + '/tmp';

  if (!fs.existsSync(tempDir)) {
    fs.mkdirSync(tempDir);
  }
};

const bytesToSize = bytes => {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

module.exports = { getFiles, bytesToSize, createTempDirectory };
