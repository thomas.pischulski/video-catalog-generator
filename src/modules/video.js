/*
 * Includes
 */
//const ffmpeg = require('ffmpeg');
const toTime = require('to-time');
const fs = require('fs');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');
const movieFileRegex = /^(?!SD_).*\.(mp4|mov)$/i;

/*
 * Functions
 */

/**
 * Takes raw video footage and renders it in low resolution with a watermark.
 *
 * @param {Object} video - Object that contains a file and the file's size
 * @param {string} watermark - Path to the watermark file
 * @param {Boolean} overwriteExisting - Parameter that stops to overwrite existing video files when set to false (default: true)
 */
const renderLowRes = (video, watermark, overwriteExisting = true) => {
  const originalPath = video.file;
  const basename = path.basename(originalPath);
  const sdFilePath = path.dirname(originalPath) + '/tmp/SD_' + basename;
  const sdFileExists = fs.existsSync(sdFilePath);
  let oldFileDeleted = false;
  if (sdFileExists && overwriteExisting) {
    fs.unlinkSync(sdFilePath);
    oldFileDeleted = true;
  }

  if (!sdFileExists || oldFileDeleted) {
    const command = ffmpeg(video.file);
    console.log(basename + ': Starting to render');
    console.time(basename);
    return command
      .input(watermark)
      .noAudio()
      .videoCodec('libx264')
      .outputOptions('-pix_fmt yuv420p')
      .complexFilter(['[0:v]scale=640:-1[bg];[bg][1:v]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2'])
      .on('end', () => {
        console.timeEnd(basename);
      })
      .save(sdFilePath);
  }
};

/**
 * Gathers info for the product catalog from a video footage file.
 *
 * @param {Object} video - Object that contains a file and the file's size
 * @returns Promise
 */
const createThumbnail = video => {
  const originalPath = video.file;
  const basename = path.basename(originalPath);
  const screenshotPath = path.dirname(originalPath) + '/tmp/';
  return new Promise(resolve => {
    ffmpeg(video.file)
      .screenshots({
        timestamps: ['50%'],
        filename: `${basename}.png`,
        folder: screenshotPath,
        size: '1080x?'
      })
      .on('end', () => {
        console.log(basename + ': Thumbnail created');
      });
  });
};

/**
 * Gathers info for the product catalog from a video footage file.
 *
 * @param {Object} video - Object that contains a file and the file's size
 * @returns Promise
 */
const getMetaData = video => {
  return new Promise(resolve =>
    ffmpeg.ffprobe(video.file, (error, metadata) => {
      const { streams, format } = metadata;
      const firstStream = streams[0];
      const data = {
        codec: firstStream.codec_long_name,
        fileFormat: format.format_long_name,
        tags: format.tags['com.apple.quicktime.keywords'],
        title: format.tags['com.apple.quicktime.title'],
        resolution: firstStream.width + 'x' + firstStream.height,
        aspectRatio: firstStream.display_aspect_ratio,
        fps: firstStream.avg_frame_rate.split('/')[0],
        length: toTime.fromSeconds(Math.round(firstStream.duration)).humanize()
      };

      resolve(data);
    })
  );
};

module.exports = { renderLowRes, getMetaData, createThumbnail, movieFileRegex };
